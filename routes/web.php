<?php

Auth::routes();

Route::delete('admin/items/{item_id}', 'MenuController@delete')->middleware('auth');

Route::get('admin/items/{item_id}', 'MenuController@load')->middleware('auth');

Route::get('admin/items','AdminController@sort')->middleware('auth');

Route::put('admin/items/{item_id}', 'MenuController@edit')->middleware('auth');

Route::post('admin/items','MenuController@add')->middleware('auth');

Route::get('/slider', 'SliderController@index')->middleware('auth');

Route::post('/add', 'SliderController@add')->middleware('auth');

Route::get('/delete/{id}', 'SliderController@delete')->middleware('auth');

Route::get('/load/{id}', 'SliderController@load')->middleware('auth');

Route::post('/edit/{id}', 'SliderController@edit')->middleware('auth');

Route::post('login/custom',[
    'uses' => 'Auth\LoginController@login',
    'as' => 'login.custom'
]);

Route::group(['middleware' => 'auth'], function (){

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('admin', 'AdminController@index')->middleware('isAdmin')->name('dashboard');

});

Route::get('/questions', 'QuestionController@index')->middleware('auth');

Route::delete('questions/{comment_id}', 'QuestionController@delete')->middleware('auth');

Route::get('questions/{comment_id}', 'QuestionController@load')->middleware('auth');

Route::put('questions/{comment_id}', 'QuestionController@edit')->middleware('auth');

Route::post('questions','QuestionController@add')->middleware('auth');

Route::get('/question/{comment_id}', 'CommentController@index')->middleware('auth');

Route::delete('question/comment_id/{comment_id}', 'CommentController@delete')->middleware('auth');

Route::get('question/comment_id/{comment_id}', 'CommentController@load')->middleware('auth');

Route::put('question/comment_id/{comment_id}', 'CommentController@edit')->middleware('auth');

Route::get('question/id/{comment_id}','CommentController@loadData' )->middleware('auth');;

Route::post('question/comment_id','CommentController@addComment')->middleware('auth');
