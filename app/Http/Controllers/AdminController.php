<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AdminController
{

    public function index()
    {
        $menu = Menu::all();
        $order = DB::table('menus')->orderBy('order')->get();
        return view('admin.dashboard', ['menus'=> $menu, 'menus'=>$order]);
    }

    public function sort(Request $request)
    {
        $itemId = $request->itemId;
        $itemIndex = $request->itemIndex;

        return  DB::table('menus')->where('id','=',$itemId)->update(array('order'=> $itemIndex));
    }
}




