<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class MenuController
{

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'url' => 'required',
        ]);

        $max = DB::table('menus')-> MAX ('order');

        if ($validator->passes()) {
            $item = Menu::create([
                'name' => $request->name,
                'url' => $request->url,
                'order' => (int)$max+1
            ]);

            return response()->json(['success'=>$item]);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function load($item_id)
    {
        $item = Menu::find($item_id);
        response()->json($item)->send();
    }

    public function edit(Request $request, $item_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'url' => 'required',
        ]);

        if ($validator->passes()) {
            $item = Menu::find($item_id);
            $item->name = $request->name;
            $item->url = $request->url;
            $item->save();
            return response()->json(['success'=>$item]);
        }

        return response()->json(['error'=>$validator->errors()->all()]);


    }

    public function delete($item_id)
    {
        $item = Menu::destroy($item_id);
        response()->json($item)->send();
    }
}