<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Slider;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $item = Menu::all();
        $slider = Slider::all();
        return view('home', ['menus'=> $item, 'slider'=> $slider]);
    }

}
