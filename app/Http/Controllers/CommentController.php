<?php
/**
 * Created by IntelliJ IDEA.
 * User: stude
 * Date: 04.10.2017
 * Time: 15:34
 */

namespace App\Http\Controllers;

use App\Comment;
use App\Question;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class CommentController
{
    public function index($comment_id){
        $user = User::all();
        $question = Question::where('id', '=', $comment_id)->get();
        $comments = Question::find($comment_id)->comments;
        $order = DB::table('comments')->orderBy('order')->offset(0)->limit(10)->where('question_id', '=', $comment_id)->get();
        return view('comment.comment', ['comments'=>$comments, 'comments'=>$order, 'questions'=>$question, 'users'=>$user]);
    }

    public function loadData(Request $request, $comment_id)
    {
        $output = '';
        $order = $request->order;
        $user = $request->user;
        $offset = $request->offset;
        $comments = Comment::where('order','>',$order)->orderBy('order')->offset($offset)->limit(2)->get();
        $questions = Question::where('id', '=', $comment_id)->get();

        if(!$comments->isEmpty()) {
            foreach($questions as $question){
            }
            foreach($comments as $comment) {
                    $nivel = $comment->nivel * 30;
                    $output .= '<div id="comment' . $comment->id . '" style="margin-left:'.$nivel.'px">
                                    <div style="float: right;">
                                        <button class="btn btn-primary btn-xs btn-detail reply-comment" value="' . $comment->id . '">Reply</button>
                                        <button class="btn btn-warning btn-xs btn-detail edit-comment" value="' . $comment->id . '">Edit</button>
                                        <button class="btn btn-danger btn-xs btn-delete delete-comment" value="' . $comment->id . '">Delete</button>
                                    </div>';
                    if ((($comment->nivel) == 2) && (($comment->reply_user) != '')){
                        $output .= '<h4><span style="font-weight: bold">'.$comment->user . ': </span>@' . $comment->reply_user . ' ' . $comment->comment . '</h4>';
                    }else{
                        $output .= '<h4><span style="font-weight: bold">'.$comment->user . ': </span>' . $comment->comment . '</h4>';
                    }
                        $output .= '<div class="form-group" id="reply' . $comment->id . '" style="display: none; margin-top: 15px">
                                        <form id="re-frmItems' . $comment->id . '" name="re-frmItems" class="form-horizontal">
                                            <div class="form-group" style="margin-right: 15px">
                                                <textarea style="margin: 0 15px 0 15px;" rows="4" id="re-comment' . $comment->id . '" class="form-control" name="comment"></textarea>
                                            </div>
                                            <div class="form-group" style="margin-right: 15px; margin-bottom: 0">
                                                <button type="button" content="' . $user . '" style="margin-left: 15px;" about="' . $comment->user . '"class="btn btn-primary btn-block" id="re-btn-save" value="' . $question->id . '" name="' . $comment->nivel . '" data-id="' . $comment->id . '">Comment<span id="re-spinner' . $comment->id . '"></span></button>
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-danger" id="re-error-msg' . $comment->id . '" style="display: none;margin-left:15px;margin-right:15px;">
                                                    <ul></ul>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <hr/>
                                </div>
                                <div id="comment-none' . $comment->id . '" style="display: none"></div>';
            }
            $output .= '<div id="remove-row">
                            <a role="button" id="btn-more" content="' . $user . '" name="'.$question->id.'" data-id="'.$comment->order.'" ><h4 class="text-center" id="loading"> Load More </h4></a>
                        </div>';
            echo $output;
        }
    }

    public function addComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);

        if ($request->parent_id == 0){
            $max = DB::table('comments')-> MAX ('order');
            $nivel = $request->nivel;

            if ($validator->passes()) {
                $comment = Comment::create([
                    'comment' => $request->comment,
                    'question_id' => $request->question_id,
                    'parent_id' => $request->parent_id,
                    'nivel' => $nivel,
                    'order' => (int)$max+1,
                    'user' => $request->user
                ]);
                return response()->json(['success'=>$comment]);
            }
        } else if ($request->parent_id >= 0){
            $position = Comment::where('id', $request->parent_id)->pluck('order');
            Comment::where('order', '>', $position)->increment('order');
            $order = $position[0] + 1;

            if($request->nivel == 2){
                $nivel = $request->nivel;
                $reply_user = $request->reply_user;
            }else{
                $nivel = $request->nivel + 1;
                $reply_user = '';
            }

            if ($validator->passes()) {
                $comment = Comment::create([
                    'comment' => $request->comment,
                    'question_id' => $request->question_id,
                    'parent_id' => $request->parent_id,
                    'nivel' => $nivel,
                    'order' => $order,
                    'user' => $request->user,
                    'reply_user' =>$reply_user
                ]);
                return response()->json(['success'=>$comment]);
            }
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function delete($comment_id)
    {
        $comment = Comment::destroy($comment_id);
        response()->json($comment)->send();
    }

    public function load($comment_id)
    {
        $comment = Comment::find($comment_id);
        response()->json($comment)->send();
    }

    public function edit(Request $request, $comment_id)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);

        if ($validator->passes()) {
            $comment = Comment::find($comment_id);
            $comment->comment = $request->comment;
            $comment->save();
            return response()->json(['success' => $comment]);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }


}