<?php
/**
 * Created by IntelliJ IDEA.
 * User: stude
 * Date: 28.09.2017
 * Time: 17:07
 */

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Validator;

class QuestionController
{
    public function index(){
        $comment = Question::all();
        $page = Question::paginate(5);

        return view('comment.question', ['comments'=> $comment, 'comments' => $page]);
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);
        if ($validator->passes()) {
            $comment = Question::create($request->all());
            return response()->json(['success'=>$comment]);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function delete($comment_id)
    {
        $comment_id = Question::destroy($comment_id);
        response()->json($comment_id)->send();
    }

    public function load($comment_id)
    {
        $comment = Question::find($comment_id);
        response()->json($comment)->send();
    }

    public function edit(Request $request, $comment_id)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);

        if ($validator->passes()) {
            $comment = Question::find($comment_id);
            $comment->comment = $request->comment;
            $comment->save();
            return response()->json(['success'=>$comment]);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }
}