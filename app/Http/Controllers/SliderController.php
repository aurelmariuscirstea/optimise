<?php
/**
 * Created by IntelliJ IDEA.
 * User: stude
 * Date: 28.09.2017
 * Time: 10:16
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Slider;

class SliderController extends Controller
{
    public function index(){
        $slider = Slider::all();
        return view('slider.slider', ['slider'=> $slider]);
    }

    public function add(Request $request){

        $request->validate([
            'name' => 'required',
            'profile_pic' => 'required',
        ]);

        $slider = new Slider();
        $slider->name = $request->input('name');
        if (Input::hasFile('profile_pic')){
            $file = Input::file('profile_pic');
            $file->move(public_path().'/uploads/', $file->getClientOriginalName());
            $url = URL::to("/"). '/uploads/'. $file->getClientOriginalName();
        }
        $slider->profile_pic = $url;
        $slider->save();
        return redirect('/slider');
    }

    public function delete($slider_id){
        Slider::where('id', $slider_id)->delete();
        return redirect('/slider');
    }

    public function load($slider_id){
        $slider = Slider::find($slider_id);
        return view('slider.edit', ['slider'=>$slider]);
    }

    public function edit(Request $request, $slider_id){

        $request->validate([
            'name' => 'required',
            'profile_pic' => 'required',
        ]);

        $slider = new Slider;
        $slider->name = $request->input('name');
        if (Input::hasFile('profile_pic')){
            $file = Input::file('profile_pic');
            $file->move(public_path().'/uploads/', $file->getClientOriginalName());
            $url = URL::to("/"). '/uploads/'. $file->getClientOriginalName();
        }
        $slider->profile_pic = $url;
        $data = array(
            'name'=>$slider->name,
            'profile_pic'=>$slider->profile_pic
        );
        Slider::where('id',$slider_id)->update($data);
        $slider->update($data);
        return redirect('/slider');
    }
}