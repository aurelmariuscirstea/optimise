<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['comment'];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
