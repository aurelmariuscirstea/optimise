<?php

/**
 * Created by IntelliJ IDEA.
 * User: stude
 * Date: 04.10.2017
 * Time: 15:41
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['comment','question_id', 'parent_id', 'nivel', 'order', 'user', 'reply_user'];

    public function question()
    {
        return $this->belongsTo('App\Comment');
    }
}
