@extends('slider.layout')

@section('content')
<style type="text/css">
    .avatarpost{
        max-width: 25px;
        max-height: 25px;
    }
    .col-sm-4 .form-group{
        margin-right: 0;
        margin-top: 8px;
    }
</style>
<div class="container-narrow">
    <div class="row">
        <div class="col-sm-8">
            <!-- Table-to-load-the-data Part -->
            <table class="table" id="sort">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Logo</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="items-list" name="items-list">
                @foreach ($slider as $slider)
                <tr id="item{{$slider->id}}">
                    <td class="index">{{$slider->id}}</td>
                    <td>{{$slider->name}}</td>
                    <td><img src="{{$slider->profile_pic}}" class="avatarpost"></td>
                    <td>
                        <button class="btn btn-warning btn-xs btn-detail edit-rows"><a href='{{url("/load/{$slider->id}")}}' style="color: white">Edit</a></button>
                        <button class="btn btn-danger btn-xs btn-delete delete-rows"><a href='{{url("/delete/{$slider->id}")}}' style="color: white">Delete</a></button>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="col-sm-4">
            <form class="form-horizontal" method="POST" action="{{ url('/add') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="name" class="form-control" name="name" value=""  >
                </div>

                <div class="form-group">
                    <label for="profile_pic">Picture</label>
                    <input id="profile_pic" type="file" class="form-control" name="profile_pic" >
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

                <div class="form-group">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
