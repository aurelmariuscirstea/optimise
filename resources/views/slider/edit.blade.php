@extends('slider.layout')

@section('content')
<style type="text/css">
    .avatarpost{
        max-width: 100px;
        max-height: 100px;
    }
</style>
<div class="container-narrow">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <form class="form-horizontal" method="POST" action="{{url('/edit', array($slider->id)) }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="name" class="form-control" name="name" value="{{ $slider->name }}">
                </div>

                <div class="form-group">
                    <label for="profile_pic">Picture</label>
                    <div>
                        <div style="float:left;">
                            <img src="{{$slider->profile_pic}}" class="avatarpost">
                        </div>
                        <div style="float: right; margin-top: 25px;">
                            <input id="profile_pic" type="file" class="form-control" name="profile_pic">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

                <div class="form-group">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </form>
        </div>
        <div class="col-sm-4"></div>
    </div>
</div>

@endsection