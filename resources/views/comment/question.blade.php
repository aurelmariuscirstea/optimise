@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">
<meta name="_token" content="{!! csrf_token() !!}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{asset('js/question.js')}}"></script>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <form id="frmItems" name="frmItems" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <textarea rows="4" id="comment" class="form-control" name="comment"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-block" id="btn-save" value="add">Comment<span id="spinner"></span></button>
                            </div>
                            <div class="form-group">
                                <div class="alert alert-danger" id="error-msg" style="display: none">
                                    <ul></ul>
                                </div>
                            </div>
                        </form>
                        <input type="hidden" id="comment_id" name="comment_id" value="0">
                        <div class="form-group" id="items-list" name="items-list">
                            @if(count($comments) > 0)
                            @foreach($comments->all() as $comment)
                            <div id="comment{{$comment->id}}">
                                <div style="float: right;">
                                    <button class="btn btn-warning btn-xs btn-detail edit-comment" value="{{$comment->id}}">Edit</button>
                                    <button class="btn btn-danger btn-xs btn-delete delete-comment" value="{{$comment->id}}">Delete</button>
                                </div>
                                <a href='{{url("/question/{$comment->id}")}}'><h4>{{$comment->comment}}</h4></a>
                            </div>
                            @endforeach
                            @else
                            <p id="no_question">No Questions Anvailable!</p>
                            @endif
                        </div>
                        {{$comments->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
