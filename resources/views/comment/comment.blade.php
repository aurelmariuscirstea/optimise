@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">
<meta name="_token" content="{!! csrf_token() !!}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{asset('js/comment.js')}}"></script>
<script src="{{asset('js/reply-comment.js')}}"></script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        @foreach($questions as $question)
                        <h3>{{$question->comment}}</h3>
                        @endforeach
                        <div id="load-data" >
                            @foreach($comments as $comment)
                            @endforeach
                            <form id="frmItems" name="frmItems" class="form-horizontal">
                                <div class="form-group" style="margin-right: 15px">
                                    <textarea style="margin: 0 15px 0 15px;" rows="4" id="comment" class="form-control" name="comment"></textarea>
                                </div>
                                <div class="form-group" style="margin-right: 15px;">
                                    <button type="button" style="margin-left: 15px;" class="btn btn-primary btn-block" name="{{ Auth::user()->name }}" id="btn-save" value="addComment"  data-id="{{$question->id}}">Comment<span id="spinner"></span></button>
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-danger" id="error-msg" style="display: none">
                                        <ul></ul>
                                    </div>
                                </div>
                            </form>
                            <input type="hidden" id="comment_id" name="comment_id" value="0">
                            <div class="form-group" id="items-list" name="items-list">
                                @if(count($comments) > 0)
                                @foreach($comments->all() as $comment)
                                <span style="display: none">{{$nivel = $comment->nivel * 30}}</span>
                                <div id="comment{{$comment->id}}" style="margin-left: {{$nivel}}px">
                                    <div style="float: right;">
                                        <button class="btn btn-primary btn-xs btn-detail reply-comment" value="{{$comment->id}}">Reply</button>
                                        <button class="btn btn-warning btn-xs btn-detail edit-comment"   value="{{$comment->id}}">Edit</button>
                                        <button class="btn btn-danger btn-xs btn-delete delete-comment" value="{{$comment->id}}">Delete</button>
                                    </div>
                                    @if((($comment->nivel) == 2) && (($comment->reply_user) != ''))
                                    <h4><span style="font-weight: bold">{{ $comment->user }}: </span><span>@</span>{{ $comment->reply_user }} {{$comment->comment}}</h4>
                                    @else
                                        <h4><span style="font-weight: bold">{{ $comment->user }}: </span>{{$comment->comment}}</h4>
                                    @endif
                                    <div class="form-group" id="reply{{$comment->id}}" style="display: none; margin-top: 15px">
                                        <form id="re-frmItems{{$comment->id}}" name="re-frmItems" class="form-horizontal">
                                            <div class="form-group" style="margin-right: 15px">
                                                <textarea style="margin: 0 15px 0 15px;" rows="4" id="re-comment{{$comment->id}}" class="form-control" name="comment"></textarea>
                                            </div>
                                            <div class="form-group" style="margin-right: 15px; margin-bottom: 0">
                                                <button content="{{ Auth::user()->name }}" about="{{ $comment->user }}" type="button" style="margin-left: 15px;" class="btn btn-primary btn-block {{ $comment->user }}" id="re-btn-save" value="{{$question->id}}" name="{{$comment->nivel}}" data-id="{{$comment->id}}">Comment<span id="re-spinner{{$comment->id}}"></span></button>
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-danger" id="re-error-msg{{$comment->id}}" style="display: none;margin-left:15px;margin-right:15px;">
                                                    <ul></ul>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <hr/>
                                </div>
                                <div id="comment-none{{$comment->id}}" style="display: none">
                                </div>
                                @endforeach
                                @else
                                <p id="no_comments">No Comments Anvailable!</p>
                                @endif
                            </div>
                            <div id="remove-row">
                                @if(count($comments) >= 10)
                                <a role="button" id="btn-more" content="{{ Auth::user()->name }}" name="{{ $question->id }}" class="{{ $comment->nivel}}" data-id="{{ $comment->order }}"><h4 class="text-center" id="loading" > Load More </h4></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
