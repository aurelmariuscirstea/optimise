<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <script src="{{ URL::asset('https://use.fontawesome.com/7dbd76657d.js') }}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="{{ URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js' ) }}"></script>
    <script src="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') }}" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}" >
    <title>Roweb</title>
</head>
<body>

<a href="#" id="back-to-top" title="Back to top">&uarr;</a>
<a name="home"></a>
<div class="intro-banner">
    <nav class="navbar navbar-default">
        <div class="container topnav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand"><img src="{{asset('img/logo.png')}}" id="logo"></a>
            </div>
            <div class="container">
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                        @foreach ($menus as $item)
                            <li><a href="{{$item->url}}"><p>{{$item->name}}</p></a></li>
                        @endforeach
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="intro-inner">
                    <h1 class="paragrap_1_home">One website to manage all your transport</h1>
                    <h1 class="paragrap_1_home_hidden">Faster shipping with Certified Drivers</h1>
                    <h3 class="paragrap_2_home">We connect shippers with carriers</h3>
                    <h3 class="paragrap_2_home_hidden">One website to manage all your logistic needs</h3>
                    <h3 class="paragrap_2_home_hidden">Built by logistics people, for logistics people</h3>
                    <ul class="list-inline intro-social-buttons">
                        <button id="button_forum" type="button">Forum</button>
                        <button id="button_profile" type="button">Profile</button>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<a name="about"></a>
<div class="container">
    <div class="row" id="carousel">
        <div class="col-sm-12">
            <section class="regular slider">
                @foreach ($slider as $slider)
                <img src="{{$slider->profile_pic}}" class="icon">
                @endforeach
            </section>
            <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
            <script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
            <script type="text/javascript">
                $(document).on('ready', function() {
                    $(".regular").slick({
                        dots: true,
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 3
                    });
                });
            </script>
        </div>
    </div>
    <div class="row" id="ship_haul">
        <div class="col-sm-5" id="ship">
            <div class="title">
                <span>Ship</span>
                <img src="{{asset('img/ship-hover.png')}}"class="img">
            </div>
            <div class="featured">
                <p class="paragrap_ship">One website to book and track.
                    Book transport in minutes.
                    Make shipping easy.</p>
                <button id="button_ship" type="button">Ship</button>
            </div>
        </div>
        <div class="col-sm-5" id="haul">
            <div class="title">
                <span>Haul</span>
                <img src="{{asset('img/haul.png')}}" class="img">
            </div>
            <div class="featured">
                <p class="paragrap_haul">More Work.</p>
                <p class="paragrap_haul">Fast Payment</p>
                <p class="paragrap_haul">No Subscriptions.</p>
                <button id="button_carry" type="button">Carry</button>
            </div>
        </div>
    </div>
</div>


<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-5">
                <hr class="section-heading-spacer1">
                <div class="clearfix"></div>
                <h2 class="paragrap_1_about">Optimise assists all participants in the supply chain to unlock value.</h2>
                <p class="paragrap_2_about">We make it easier for shippers to book and track trucks.</p>
                <p class="paragrap_2_about"> We make it easier for trucks to find work.</p>
                <p class="paragrap_2_about"> We allocate loads to empty space.</p>
                <p class="paragrap_2_about">Real-time visibility of all trucks on one platform, communicate with the click of a finger.</p>
                <p class="paragrap_2_about"> Our in-house technology removes manual processing allowing us to pass on savings to you.</p>
            </div>
            <img class="img-responsive img-rounded" src="{{asset('img/screen.png')}}">
        </div>
    </div>
</div>


<a name="features"></a>
<div class="features">
    <div class="intro-banner">
        <div class="container">
            <div class="row">
                <div class="col-sm-5" id="features_left">
                    <img src="{{asset('img/video.png')}}" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
                </div>
                <div class="col-sm-5" id="features_right">
                    <hr class="section-heading-spacer1">
                    <div class="clearfix"></div>
                    <h2 class="paragrap_1_features">Ship with Optimise</h2>
                    <p class="paragrap_2_features">Fast, Reliable, Great Rates, Total Visibility</p>
                    <p class="paragrap_2_features_hidden">Efficient, Reliable and Data-driven</p>
                </div>
                <img src="{{asset('img/video.png')}}" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" id="modal_video">
            </div>
        </div>
    </div>


    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <video controls>
                            <source src="{{asset('video/video.mp4')}}" type="video/mp4">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row" >
            <div class="col-sm-12" >
                <hr class="section-heading-spacer2">
                <div class="title" id="title_features">
                    <span>More Work, Fast Payment</span>
                </div>
                <div class="title" id="title_features_hidden">
                    <span>Fast, Reliable, Great Rates, Total Visibility</span>
                </div>
                <div class="featured" id="paragrap_3_features">
                    <p>Sign up, log into our easy to use webpage to book trucks instantly, track all your loads real time, instant notifications.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container" >
        <div class="row">
            <div class="col-md-3" >
                <img src="{{asset('img/Umbrella.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Great Rates</span>
                </div>
                <div class="featured">
                    <p>Move loads in unused capacity. Our highly automated technology allows us to pass on savings to you.</p>
                </div>
            </div>
            <div class="col-md-3 hidden-xs">
                <img src="{{asset('img/Clock.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Save Time</span>
                </div>
                <div class="featured">
                    <p>Find loads instantly. View every carrier on one single dashboard, control your transport with a click.</p>
                </div>
            </div>
            <div class="col-md-3 hidden-xs">
                <img src="{{asset('img/Badge.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Reliable</span>
                </div>
                <div class="featured">
                    <p>Certified trusted drivers. Insurance, licensing and businesss references  verified. Rated by shippers like you.</p>
                </div>
            </div>
            <div class="col-md-3 hidden-xs">
                <img src="{{asset('img/Eyeglass.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Fully Visibility</span>
                </div>
                <div class="featured">
                    <p>Track loads real time, notifications sent to keep you up to date.</p>
                    <p class="p">Track loads real time.</p>
                </div>
            </div>
            <div class="col-md-3 hidden-xs">
                <img src="{{asset('img/Wallet.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Fair Price</span>
                </div>
                <div class="featured">
                    <p>Fair market rates calculated by us. No reverse bidding or haggling.</p>
                </div>
            </div>
            <div class="col-md-3 hidden-xs">
                <img src="{{asset('img/Speedometer.png')}}" class="icon_col-3">
                <div class="title">
                    <span></span>Fast Payment
                </div>
                <div class="featured">
                    <p>Receive payment in just a few short days. No more chasing outstanding invoices.</p>
                </div>
            </div>
            <div class="col-md-3 hidden-xs">
                <img src="{{asset('img/Directions.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Track</span>
                </div>
                <div class="featured">
                    <p>Track all your trucks realtime on our mobile app or webpage.</p>
                </div>
            </div>
            <div class="col-md-3 hidden-xs">
                <img src="{{asset('img/User.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Less Admin</span>
                </div>
                <div class="featured">
                    <p>Documentation automatically produced, less resources tied up.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container_features_hidden">
    <div class="container" >
        <div class="row">
            <div class="col-md-12" >
                <hr class="section-heading-spacer">
                <div class="title" id="title_features_hidden">
                    <span>More Work, Fast Payment</span>
                </div>
                <div class="featured" id="paragrap_3_features_hidden">
                    <span>No Monthly Subscriptions No Reverse Bidding</span>
                </div>
            </div>
            <div class="col-md-3">
                <img src="{{asset('img/Wallet.png')}}" class="icon_col-3">
                <div class="title">
                    <span>Fair Price</span>
                </div>
                <div class="featured" >
                    <p >Fair market rates calculated by us. No reverse bidding or haggling. Gain extra shipments to fill empty space.</p>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container-fluid"  id="container_features">
    <div class="container">
        <div class="row" >
            <div class="col-md-12">
                <div class="title">
                    <span >Talk to our team</span>
                </div>
                <div class="featured">
                    <p>No more wasted time baby sitting loads, track everything real time and order trucks in seconds</p>
                </div>
                <button id="button_talk_to_our_team" type="button">Talk to our team</button>
            </div>
        </div>
    </div>
</div>

<a name="contact"></a>
<div class="container-fluid" id="contact">
    <div class="container">
        <div class="row " >
            <div class="col-sm-12">
                <hr class="section-heading-spacer3">
                <div class="title" id="paragrap_1_contact">
                    <span>Contact us to get started</span>
                </div>
                <div class="featured">
                    <p id="paragrap_2_contact">Drop us an email at</p>
                    <p id="paragrap_3_contact">sales@optimise.com</p>
                    <p id="paragrap_4_contact">or use the formular below</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="title">
                    <lable>Name</lable>
                </div>
                <div class="featured">
                    <input type="text">
                </div>
            </div>
            <div class="col-md-4">
                <div class="title">
                    <lable>Email</lable>
                </div>
                <input type="text">
            </div>
            <div class="col-md-4">
                <div class="title">
                    <lable>Company</lable>
                </div>
                <input id="input" type="text">
            </div>
            <div class="col-md-12">
                <div class="title">
                    <lable>Additional Details</lable>
                </div>
                <input type="text" id="add">
            </div>
            <button id="button_get_in_touch" type="button">Get in touch</button>
        </div>
    </div>
</div>


<div class="container-fluid"  id="address">
    <div class="container">
        <div class="row" >
            <div class="col-md-4">
                <img src="{{asset('img/Phone.png')}}">
                <div class="title">
                    <label>Phone</label>
                </div>
                <div class="featured">
                    <p>UK: +44 (0)20 8935 5460</p>
                    <p>Europe: +353 (0)1 910 9978</p>
                    <p id="paragraph_hidden">Europe: +353 (0)1 910 9978</p>
                    <p id="paragraph_hidden">Europe: +353 (0)1 910 9978</p>
                </div>
            </div>

            <div class="col-md-4">
                <img src="{{asset('img/Location.png')}}">
                <div class="title">
                    <label>UK Address</label>
                </div>
                <div class="featured">
                    <p>Optimise</p>
                    <p>Cutlers Court</p>
                    <p>115 Houndsditch</p>
                    <p>London EC3A 7BR</p>
                </div>
            </div>

            <div class="col-md-4">
                <img src="{{asset('img/Location.png')}}">
                <div class="title">
                    <label>Headquarters</label>
                </div>
                <div class="featured">
                    <p>Optimise</p>
                    <p>D5 Riverview Business Park</p>
                    <p>Nangor Road</p>
                    <p>Dublin 12</p>
                </div>
            </div>
            <div class="address_hidden">
                <img src="{{asset('img/02_facebook.png')}}">
                <img src="{{asset('img/07_linkedin.png')}}">
            </div>
        </div>
    </div>
</div>


<footer id="footer">
    <div class="footer-copyright">
        <div class="container">
            <p>© 2017 Optimise</p>
        </div>
    </div>
</footer>

<script type="text/javascript">
    if ($('#back-to-top').length) {
        var scrollTrigger = 100,
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
</script>
</body>
</html>
