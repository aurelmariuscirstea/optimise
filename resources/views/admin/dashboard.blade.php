@extends('admin.layout')
@section('content')
<style>
    .col-sm-4 .form-group{
        margin-right: 0;
        margin-top: 8px;
    }
</style>
<div class="container-narrow">
    <div class="row">
        <div class="col-sm-8">
            <!-- Table-to-load-the-data Part -->
            <table class="table">
                <thead>
                <tr>
                    <th>Position</th>
                    <th>Name</th>
                    <th>URL</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="items-list" name="items-list">
                @foreach ($menus as $item)
                <tr id="{{$item->id}}">
                    <td class="index">{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->url}}</td>
                    <td>
                        <button class="btn btn-warning btn-xs btn-detail edit-rows" value="{{$item->id}}">Edit</button>
                        <button class="btn btn-danger btn-xs btn-delete delete-rows" value="{{$item->id}}">Delete</button>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <!-- End of Table-to-load-the-data Part -->
        <!-- Modal (Pop up when detail button clicked) -->
        <div class="col-sm-4">
            <form id="frmItems" name="frmItems" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group error">
                    <label>Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="">
                </div>

                <div class="form-group">
                    <label>URL</label>
                    <input type="text" class="form-control" id="url" name="url" placeholder="URL:" value="">
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-primary" id="btn-save" value="add">Save<span id="spinner"></span></button>
                </div>

                <div class="form-group">
                    <div class="alert alert-danger" id="error-msg" style="display: none">
                        <ul></ul>
                    </div>
                </div>
            </form>

            <input type="hidden" id="item_id" name="item_id" value="0">
        </div>
    </div>
</div>
@endsection