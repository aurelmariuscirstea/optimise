$(document).ready(function(){
    var url = "/question/comment_id";
    var offset = 0;
    var state = "addComment";

    $(document).on('click','.reply-comment',function () {
        var comment_id = $(this).val();
        $("#reply" + comment_id).toggle();
    });

    $(document).on('click','.edit-comment',function(){
        var comment_id = $(this).val();
        $.get(url + '/' + comment_id, function (data) {
            console.log(data);
            $('#comment_id').val(data.id);
            $("#re-comment" + comment_id).val(data.comment);
            $("#reply" + comment_id).toggle();
            state = "update";
        })
    });

    $(document).on('click','#re-btn-save',function () {
        var comment_id = $(this).attr('data-id');
        var question_id = $(this).attr('value');
        var nivel = $(this).attr('name');
        var user = $(this).attr('content');
        var reply_user = $(this).attr('about');

        if (nivel == 0){
            var ordernivel = 30;
        }
        if (nivel >= 1){
            var ordernivel = 60;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $('#re-spinner' + comment_id).append('<img src="/img/spinner.gif" height="25px" width="25px">');

        var formData = {
            comment: $('#re-comment' + comment_id).val(),
            question_id: question_id,
            parent_id: comment_id,
            nivel: nivel,
            user: user,
            reply_user: reply_user
        }

        var type = "POST";
        var my_url = url;
        if (state == "update"){
            type = "PUT";
            my_url += '/' + comment_id;

            if (nivel == 0){
                var ordernivel = 0;
            }else if (nivel == 1){
                var ordernivel = 30;
            }else{
                var ordernivel = 60;
            }
        }

        console.log(formData);
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                if($.isEmptyObject(data.error)) {
                    console.log(data);
                    offset++;

                    var comment = '<div  style="margin-left: '+ordernivel+'px" id="comment' + data.success.id + '">';
                    comment += '<div style="float: right">' + '<button class="btn btn-primary btn-xs btn-detail reply-comment" value="' + data.success.id + '">Reply</button>';
                    comment += '<button class="btn btn-warning btn-xs btn-detail edit-comment" style="margin-left: 4px" value="' + data.success.id + '">Edit</button>';
                    comment += '<button class="btn btn-danger btn-xs btn-detail delete-comment" style="margin-left: 4px" value="' + data.success.id + '">Delete</button></div>';
                    if ((nivel == 2) && (data.success.reply_user != null)){
                        comment += '<h4><span style="font-weight: bold">'+data.success.user+':</span> @'+data.success.reply_user +' '+data.success.comment+'</h4>';
                    }else{
                        comment += '<h4><span style="font-weight: bold">'+data.success.user+':</span> '+data.success.comment+'</h4>';
                    }
                    comment += '<div class="form-group" id="reply'+ data.success.id +'" style="margin-top: 15px; display: none">';
                    comment += '<form id="re-frmItems'+data.success.id+'" name="re-frmItems" class="form-horizontal">';
                    comment += '<div class="form-group" style="margin-right: 15px">';
                    comment += '<textarea style="margin: 0 15px 0 15px; width: 100%" rows="4" id="re-comment'+data.success.id+'" class="form-group" name="comment"></textarea></div>';
                    comment += '<div class="form-group" style="margin-right: 15px; margin-bottom: 0">';
                    comment += '<button type="button" content="'+data.success.user+'" style="margin-left: 15px" class="btn btn-primary btn-block" id="re-btn-save" value="'+question_id+'" name="'+data.success.nivel+'" data-id="'+data.success.id+'">Comment<span id="re-spinner'+data.success.id+'"></span></button></div>'
                    comment += '<div class="form-group"><div class="alert alert-danger" id="re-error-msg'+data.success.id+'" style="display: none;margin-left:15px;margin-right:15px;"><ul></ul></div></div>';
                    comment += '</form></div><hr/</div>';
                    comment += '<div  style="display: none" id="comment-none' + data.success.id + '"></div>';

                    if (state == "addComment"){
                        $('#comment-none' + comment_id).replaceWith(comment);
                    }else{
                        $('#comment' + comment_id).replaceWith(comment);
                    }

                    $('#comment-none' + comment_id).css('display','block');
                    $( "#no_question" ).hide();
                    $( '#re-spinner' + comment_id ).hide();
                    $('#reply' + comment_id).hide();

                }else{
                    ErrorMsg(data.error);
                }
            },
            error: function (data) {
                alert('Error');
                $( '#re-spinner' + comment_id ).hide();
                console.log('Error:', data);
            }
        });
        function ErrorMsg (msg) {
            $('#re-error-msg' + comment_id ).find("ul").html('');
            $('#re-error-msg' + comment_id).css('display','block');
            $.each( msg, function( key, value ) {
                $('#re-error-msg' + comment_id).find("ul").append('<li>'+value+'</li>');
            });
            if ($( '#re-comment' + comment_id ).val() === '') {
                $( '#re-comment' + comment_id ).css('border', '1px solid #dd4b39');
            } else {
                $( '#re-comment' + comment_id ).css('border', '1px solid #d2d6de');
            }
            $( '#re-spinner' + comment_id ).hide();
        }
    });

    $(document).on('click','#btn-more',function(){
        var order = $(this).data('id');

        $("#loading").html("Loading...");
        var url = "/question/id";
        var comment_id = $(this).attr('name');
        var user = $(this).attr('content');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $.ajax({
            url : url + '/' + comment_id,
            type : "GET",
            data : {
                order:order,
                user:user,
                offset:offset
            },
            dataType : 'text',
            success : function (data)
            {
                if(data != '') {
                    $('#remove-row').remove();
                    $('#load-data').append(data);
                } else {
                    $('#loading').hide();
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
});


