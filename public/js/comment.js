$(document).ready(function(){

    var url = "/question/comment_id";

    $(document).on('click','.delete-comment',function(){
        var comment_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $.ajax({
            type: "DELETE",
            url: url + '/' + comment_id,
            success: function (data) {
                console.log(data);
                $("#comment" + comment_id).remove();

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $(document).on('click','#btn-save',function () {
        var question_id = $(this).attr('data-id');
        var user = $(this).attr('name');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $('#spinner').append('<img src="/img/spinner.gif" height="25px" width="25px">');

        var formData = {
            comment: $('#comment').val(),
            question_id: question_id,
            parent_id: 0,
            nivel: 0,
            user: user
        }

        console.log(formData);
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                if($.isEmptyObject(data.error)) {
                    console.log(data);

                    $("#comment").css('border', '1px solid #d2d6de');

                    var comment = '<div id="comment' + data.success.id + '">';
                    comment += '<div style="float: right">' + '<button class="btn btn-primary btn-xs btn-detail reply-comment" value="' + data.success.id + '">Reply</button>';
                    comment += '<button class="btn btn-warning btn-xs btn-detail edit-comment" style="margin-left: 4px" value="' + data.success.id + '">Edit</button>';
                    comment += '<button class="btn btn-danger btn-xs btn-detail delete-comment" style="margin-left: 4px" value="' + data.success.id + '">Delete</button></div>';
                    comment += '<h4><span style="font-weight: bold">'+data.success.user+': </span>'+data.success.comment+'</h4>';
                    comment += '<div class="form-group" id="reply'+ data.success.id +'" style="margin-top: 15px; display: none">';
                    comment += '<form id="re-frmItems'+data.success.id+'" name="re-frmItems" class="form-horizontal">';
                    comment += '<div class="form-group" style="margin-right: 15px">';
                    comment += '<textarea style="margin: 0 15px 0 15px; width: 100%" rows="4" id="re-comment'+data.success.id+'" class="form-group" name="comment"></textarea></div>';
                    comment += '<div class="form-group" style="margin-right: 15px; margin-bottom: 0">';
                    comment += '<button type="button" content="'+data.success.user+'" style="margin-left: 15px" class="btn btn-primary btn-block" id="re-btn-save" value="'+question_id+'" name="'+data.success.nivel+'" data-id="'+data.success.id+'">Comment<span id="re-spinner'+data.success.id+'"></span></button></div>'
                    comment += '<div class="form-group"><div class="alert alert-danger" id="re-error-msg'+data.success.id+'" style="display: none;margin-left:15px;margin-right:15px;"><ul></ul></div></div>';
                    comment += '</form></div><hr/></div>';
                    comment += '<div id="comment-none' + data.success.id + '" style="display: none"></div>';

                    $('#items-list').append(comment);

                    $('#frmItems').trigger("reset");
                    $( "#error-msg" ).hide();
                    $( "#spinner" ).hide();
                    $( "#no_comments" ).hide();
                }else{
                    ErrorMsg(data.error);
                }
            },
            error: function (data) {
                alert('Error');
                $( "#spinner" ).hide();
                console.log('Error:', data);
            }
        });
    });
    function ErrorMsg (msg) {
        $("#error-msg").find("ul").html('');
        $("#error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $("#error-msg").find("ul").append('<li>'+value+'</li>');
        });

        if ($("#comment").val() === '') {
            $("#comment").css('border', '1px solid #dd4b39');
        } else {
            $("#comment").css('border', '1px solid #d2d6de');
        }
        $( "#spinner" ).hide();
    }
});