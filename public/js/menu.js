$(document).ready(function(){

    var url = "admin/items";

    $(document).on('click','.edit-rows',function(){
        var item_id = $(this).val();
        $.get(url + '/' + item_id, function (data) {
            console.log(data);
            $('#item_id').val(data.id);
            $('#name').val(data.name);
            $('#url').val(data.url);
            $('#btn-save').val("update");
        })
    });


    $(document).on('click','.delete-rows',function(){
        var item_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $.ajax({
            type: "DELETE",
            url: url + '/' + item_id,
            success: function (data) {
                $("#" + item_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    $(document).on('click','#btn-save',function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        $('#spinner').append('<img src="/img/spinner.gif" height="25px" width="25px">');


        var state = $('#btn-save').val();
        var type = "POST";
        var item_id = $('#item_id').val();
        var my_url = url;
        var formData = {
            name: $('#name').val(),
            url: $('#url').val(),
        }

        if (state == "update"){
            type = "PUT";
            my_url += '/' + item_id;
        }
        console.log(formData);

        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {

                if($.isEmptyObject(data.error)){
                    console.log(data);

                    $("#url").css('border', '1px solid #d2d6de');
                    $("#name").css('border', '1px solid #d2d6de');

                    var rows = '<tr id="' + data.success.id + '"><td>' + data.success.id + '</td><td>' + data.success.name + '</td><td>' + data.success.url + '</td>';
                    rows += '<td><button class="btn btn-warning btn-xs btn-detail edit-rows" style="margin-right: 3px" value="' + data.success.id + '">Edit</button>';
                    rows += '<button class="btn btn-danger btn-xs btn-delete delete-rows" value="' + data.success.id + '">Delete</button></td></tr>';


                    if (state == "add"){
                        $('#items-list').append(rows);
                    }else{
                        $("#" + item_id).replaceWith(rows);
                    }

                    $('#frmItems').trigger("reset");
                    $( "#error-msg" ).hide();
                    $( "#spinner" ).hide();

                }else{
                    ErrorMsg(data.error);
                }

            },
            error: function (data) {
                alert('Error');
                $( "#spinner" ).hide();
                console.log('Error:', data);
            }
        });
    });
    function ErrorMsg (msg) {
        $("#error-msg").find("ul").html('');
        $("#error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $("#error-msg").find("ul").append('<li>'+value+'</li>');
        });

        if ($("#name").val() === '') {
            $("#name").css('border', '1px solid #dd4b39');
        } else {
            $("#name").css('border', '1px solid #d2d6de');
        }

        if ($("#url").val() === '') {
            $("#url").css('border', '1px solid #dd4b39');
        } else {
            $("#url").css('border', '1px solid #d2d6de');
        }

        $( "#spinner" ).hide();
    }
});

$(document).ready(function(){
    $(function () {
        $( "tbody" ).sortable({
            stop: function () {
                $.map($(this).find('tr'), function (ev) {
                    var itemId = ev.id;
                    var itemIndex = $(ev).index();
                    $.ajax({
                        url: "admin/items",
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            itemId: itemId,
                            itemIndex: itemIndex
                        },
                        success: function (data) {

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    })
                });
            }
        });
    });
});


