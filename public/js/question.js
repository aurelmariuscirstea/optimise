$(document).ready(function(){

    var url = "/questions";

    $(document).on('click','.edit-comment',function(){
        var comment_id = $(this).val();
        $.get(url + '/' + comment_id, function (data) {
            console.log(data);
            $('#comment_id').val(data.id);
            $('#comment').val(data.comment);
            $('#btn-save').val("update");
        })
    });

    $(document).on('click','.delete-comment',function(){
        var comment_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $.ajax({
            type: "DELETE",
            url: url + '/' + comment_id,
            success: function (data) {
                console.log(data);
                $("#comment" + comment_id).remove();

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $(document).on('click','#btn-save',function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        $('#spinner').append('<img src="/img/spinner.gif" height="25px" width="25px">');

        var formData = {
            comment: $('#comment').val(),
        }
        var state = $('#btn-save').val();
        var type = "POST";
        var my_url = url;
        var comment_id = $('#comment_id').val();

        if (state == "update"){
            type = "PUT";
            my_url += '/' + comment_id;
        }

        console.log(formData);
        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                if($.isEmptyObject(data.error)) {
                    console.log(data);

                    $("#comment").css('border', '1px solid #d2d6de');

                    var comment = '<div id="comment' + data.success.id + '">';
                    comment += '<div style="float: right">'+'<button class="btn btn-warning btn-xs btn-detail edit-comment" value="' + data.success.id + '">Edit</button>';
                    comment += '<button class="btn btn-danger btn-xs btn-detail delete-comment" style="margin-left: 4px" value="' + data.success.id + '">Delete</button></div>';
                    comment += '<a href=""><h4>'+ data.success.comment + '</h4></a></div>';

                    if (state == "add"){
                        $('#items-list').append(comment);
                    }else{
                        $("#comment" + comment_id).replaceWith(comment);
                    }

                    $('#frmItems').trigger("reset");
                    $( "#error-msg" ).hide();
                    $( "#no_question" ).hide();
                    $( "#spinner" ).hide();
                }else{
                    ErrorMsg(data.error);

                }
            },
            error: function (data) {
                alert('Error');
                $( "#spinner" ).hide();
                console.log('Error:', data);
            }
        });
    });
    function ErrorMsg (msg) {
        $("#error-msg").find("ul").html('');
        $("#error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $("#error-msg").find("ul").append('<li>'+value+'</li>');
        });

        if ($("#comment").val() === '') {
            $("#comment").css('border', '1px solid #dd4b39');
        } else {
            $("#comment").css('border', '1px solid #d2d6de');
        }

        $( "#spinner" ).hide();
    }
});